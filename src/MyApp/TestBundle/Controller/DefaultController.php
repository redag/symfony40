<?php

namespace MyApp\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TestBundle:Default:index.html.twig');
    }
	public function pageAction()
	{
        return $this->render('TestBundle:Default:page_une.html.twig');
	}
	public function layoutAction()
	{
        return $this->render('TestBundle::layout.html.twig');
	}
	public function indextemplateAction()
	{
        return $this->render('TestBundle:template:index.html.twig');
	}
	public function widgetsAction()
	{
        return $this->render('TestBundle:template:widgets.html.twig');
	}
	public function uploadAction()
	{
        return $this->render('TestBundle:template:upload.html.twig');
	}
}
